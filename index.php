<pre>
    <?

    set_time_limit(120);

    // Filters start
    $aktarma    = ''; // false or true or empty for hepsi
    $kalkiskod  = 'IST'; // ex. for Istanbul Ataturk = IST
    $variskod   = 'GYD'; // ex. for Rusya Moskova Sheremetyevo = SVO
    $gidisdonus = 2; // 1 or 2
    // Filters end


   $baglanti         = new SoapClient('http://88.247.60.172/WSTEST/Service.asmx?wsdl');
    $cevap_xml_string = $baglanti->StrIslet((object)[
        'strislem' => '<Sefer>
      <FirmaNo>1100</FirmaNo>
      <KalkisAdi>' . $kalkiskod . '</KalkisAdi>
      <VarisAdi>' . $variskod . '</VarisAdi>
      <Tarih>2017-12-29</Tarih>
      <DonusTarih>2017-12-31</DonusTarih>
      <SeyahatTipi>' . $gidisdonus . '</SeyahatTipi>
      <IslemTipi>1</IslemTipi>
      <YetiskinSayi>1</YetiskinSayi>
      <CocukSayi>0</CocukSayi>
      <BebekSayi>0</BebekSayi>
      </Sefer>',
        'stryetki' => '<Kullanici><Adi>ucakbiletiuzmaninetWS</Adi><Sifre>1672845ws8243</Sifre></Kullanici>',
    ]);

    $cevap_xml_object = simplexml_load_string($cevap_xml_string->StrIsletResult);
    $array            = json_decode(json_encode($cevap_xml_object), 1);

    require_once 'vendor/autoload.php';
    use \YaLinqo\Enumerable;

    $xml   = 'flights.xml';
   // $array = json_decode(json_encode(simplexml_load_file($xml)), 1);


    $Secenekler = from($array['Secenekler'])->orderBy('$sec ==> $sec["VFiyat"]')->toArray();

    print_r($Secenekler);

    $akt = '';

    if(empty($aktarma))
    {
        $akt = ' OR $seg["Aktarma"] !== "null"';
    }

    if ($gidisdonus == 2)
    {
        $DonusSegmentler = from($array['DonusSegmentler'])
            ->where('$seg ==> $seg["Aktarma"] == "' . var_export($aktarma, 1) . '"' . $akt . '')
            //->where('$seg ==> $seg["KalkisKod"] == "' . $variskod . '"')
            //->where('$seg ==> $seg["VarisKod"] == "' . $kalkiskod . '"')
            ->join(from($Secenekler), '$seg ==> $seg["SecenekID"]', '$sec ==> $sec["ID"]')
            ->toArray();

        print_r($DonusSegmentler);

        foreach ($DonusSegmentler as $value)
        {

            $Segment = from($array['Segmentler'])
                ->where('$seg ==> $seg["Aktarma"] == "' . var_export($aktarma, 1) . '"' . $akt . '')
                ->where('$seg ==> $seg["SecenekID"] == "' . $value[0]['SecenekID'] . '"')
                //->where('$seg ==> $seg["KalkisKod"] == "' . $kalkiskod . '"')
                //->where('$seg ==> $seg["VarisKod"] == "' . $variskod . '"')
                ->join(from($Secenekler), '$seg ==> $seg["SecenekID"]', '$sec ==> $sec["ID"]')
                ->toArray();

            //print_r($Segment);

            if (!empty($Segment))
            {
                $hours = floor($Segment[$value[0]['SecenekID']][0]['ToplamSeyahatSuresi'] / 60);
                $mins  = $Segment[$value[0]['SecenekID']][0]['ToplamSeyahatSuresi'] % 60;
                $time  = $hours > 0 ? $hours . ' Saat' . ($mins > 0 ? ' ' . $mins . ' dk' : '') : $mins . ' dk';

                ?>
                <table border="1" style="text-align: center" width="800px">
                    <tr>
                        <td><?=$Segment[$value[0]['SecenekID']][0]['HavaYolu']?>
                            <br/> <?=$Segment[$value[0]['SecenekID']][0]['Sinif']?> / <?=$Segment[$value[0]['SecenekID']][0]['SinifTip']?></td>
                        <td><?=date('d.m.Y / H:i', strtotime($Segment[$value[0]['SecenekID']][0]['KalkisTarih']))?>
                            <br/>
                            <?=$Segment[$value[0]['SecenekID']][0]['KalkisSehir']?>
                            <br/>
                            <?=$Segment[$value[0]['SecenekID']][0]['KalkisKod']?>
                        </td>
                        <td>&xrarr;
                            <br/>
                            <?=$time?>
                        </td>
                        <td><?=date('d.m.Y / H:i', strtotime($Segment[$value[0]['SecenekID']][0]['VarisTarih']))?>
                            <br/> <?=$Segment[$value[0]['SecenekID']][0]['VarisSehir']?>
                            <br/> <?=$Segment[$value[0]['SecenekID']][0]['VarisKod']?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><?=$value[1]['VFiyat']?> TL</td>
                    </tr>
                    <?
                    $hours = floor($value[0]['ToplamSeyahatSuresi'] / 60);
                    $mins  = $value[0]['ToplamSeyahatSuresi'] % 60;
                    $time  = $hours > 0 ? $hours . ' Saat' . ($mins > 0 ? ' ' . $mins . ' dk' : '') : $mins . ' dk';
                    ?>
                    <tr>
                        <td><?=$value[0]['HavaYolu']?> <br/> <?=$value[0]['Sinif']?> / <?=$value[0]['SinifTip']?></td>
                        <td><?=date('d.m.Y / H:i', strtotime($value[0]['KalkisTarih']))?>
                            <br/>
                            <?=$value[0]['KalkisSehir']?>
                            <br/>
                            <?=$value[0]['KalkisKod']?>
                        </td>
                        <td>&xrarr;
                            <br/>
                            <?=$time?>
                        </td>
                        <td><?=date('d.m.Y / H:i', strtotime($value[0]['VarisTarih']))?>
                            <br/> <?=$value[0]['VarisSehir']?>
                            <br/> <?=$value[0]['VarisKod']?>
                        </td>
                    </tr>
                 </table>
                <?
            }
        }
    }
    else
    {
        $Segmentler = from($array['Segmentler'])
            ->where('$seg ==> $seg["Aktarma"] == "' . var_export($aktarma, 1) . '"' . $akt . '')
            //->where('$seg ==> $seg["KalkisKod"] == "' . $kalkiskod . '"')
            //->where('$seg ==> $seg["VarisKod"] == "' . $variskod . '"')
            ->join(from($Secenekler), '$seg ==> $seg["SecenekID"]', '$sec ==> $sec["ID"]')
            ->toArray();

        print_r($Segmentler);

        foreach ($Segmentler as $value)
        {
            $hours = floor($value[0]['ToplamSeyahatSuresi'] / 60);
            $mins  = $value[0]['ToplamSeyahatSuresi'] % 60;
            $time  = $hours > 0 ? $hours . ' Saat' . ($mins > 0 ? ' ' . $mins . ' dk' : '') : $mins . ' dk';

            ?>
            <table border="1" style="text-align: center" width="800px">
            <tr>
                <td><?=$value[0]['HavaYolu']?> <br/> <?=$value[0]['Sinif']?> / <?=$value[0]['SinifTip']?></td>
                <td><?=date('d.m.Y / H:i', strtotime($value[0]['KalkisTarih']))?>
                    <br/>
                    <?=$value[0]['KalkisSehir']?>
                    <br/>
                    <?=$value[0]['KalkisKod']?>
                </td>
                <td>&xrarr;
                    <br/>
                    <?=$time?>
                </td>
                <td><?=date('d.m.Y / H:i', strtotime($value[0]['VarisTarih']))?>
                    <br/> <?=$value[0]['VarisSehir']?>
                    <br/> <?=$value[0]['VarisKod']?>
                </td>
            </tr>
            <tr>
                <td colspan="4"><?=$value[1]['VFiyat']?> TL</td>
            </tr>
        </table>
            <?
        }
    }

    ?>

</pre>
